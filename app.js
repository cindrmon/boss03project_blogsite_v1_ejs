// linter silencer, for some reason
//jshint esversion:6

// module imports ---------------------------------------

    const express = require("express");
    const bodyParser = require("body-parser");
    const ejs = require("ejs");
    const _ = require("lodash");

// ------------------------------------------------------

// express to app init ----------------------------------

    const app = express();

    app.set('view engine', 'ejs');

    app.use(bodyParser.urlencoded({extended: true}));
    app.use(express.static("public"));

// ------------------------------------------------------

// custom global variables ------------------------------

    const Port = process.env.PORT || 3000;
    
    const JournalPosts = [];

    const ContentSize = 50;

    const homeStartingContent = "Lacus vel facilisis volutpat est velit egestas dui id ornare. Semper auctor neque vitae tempus quam. Sit amet cursus sit amet dictum sit amet justo. Viverra tellus in hac habitasse. Imperdiet proin fermentum leo vel orci porta. Donec ultrices tincidunt arcu non sodales neque sodales ut. Mattis molestie a iaculis at erat pellentesque adipiscing. Magnis dis parturient montes nascetur ridiculus mus mauris vitae ultricies. Adipiscing elit ut aliquam purus sit amet luctus venenatis lectus. Ultrices vitae auctor eu augue ut lectus arcu bibendum at. Odio euismod lacinia at quis risus sed vulputate odio ut. Cursus mattis molestie a iaculis at erat pellentesque adipiscing.";
    const aboutContent = "Hac habitasse platea dictumst vestibulum rhoncus est pellentesque. Dictumst vestibulum rhoncus est pellentesque elit ullamcorper. Non diam phasellus vestibulum lorem sed. Platea dictumst quisque sagittis purus sit. Egestas sed sed risus pretium quam vulputate dignissim suspendisse. Mauris in aliquam sem fringilla. Semper risus in hendrerit gravida rutrum quisque non tellus orci. Amet massa vitae tortor condimentum lacinia quis vel eros. Enim ut tellus elementum sagittis vitae. Mauris ultrices eros in cursus turpis massa tincidunt dui.";
    const contactContent = "Scelerisque eleifend donec pretium vulputate sapien. Rhoncus urna neque viverra justo nec ultrices. Arcu dui vivamus arcu felis bibendum. Consectetur adipiscing elit duis tristique. Risus viverra adipiscing at in tellus integer feugiat. Sapien nec sagittis aliquam malesuada bibendum arcu vitae. Consequat interdum varius sit amet mattis. Iaculis nunc sed augue lacus. Interdum posuere lorem ipsum dolor sit amet consectetur adipiscing elit. Pulvinar elementum integer enim neque. Ultrices gravida dictum fusce ut placerat orci nulla. Mauris in aliquam sem fringilla ut morbi tincidunt. Tortor posuere ac ut consequat semper viverra nam libero.";

// ------------------------------------------------------

// app.get commands -------------------------------------
    // app.get to home/root
        app.get( '/', (req,res) => {

            res.render('home', {

              HomeContent: homeStartingContent,
              BlogPosts: JournalPosts,
              ContentSizeInHome: ContentSize,
            });

        });

    // app.get to home/posts/:postName
        app.get('/posts/:PostName', (req,res) => {

            let PostNameInLink = _.kebabCase(req.params.PostName);

            JournalPosts.forEach(JournalPost => {

              let PostNameInArray = JournalPost.URL;

              if(PostNameInLink === PostNameInArray)
                  res.render('post', {

                      PostIndividualTitle: JournalPost.Title,
                      PostIndividualContent: JournalPost.Content

                  }); // inserts post title and post content into a post that is assigned a link

            }); // assigns a specific post to a specific link that is acquired from req.params.PostName variable


        });

    // app.get to about
        app.get( '/about', (req,res) => {

            res.render('about', {

              AboutContent: aboutContent

            })

        });

    // app.get to contact
        app.get( '/contact', (req,res) => {

            res.render('contact', {

                ContactContent: contactContent

            });

        });

    // app.get to compose (hidden)
        app.get( '/compose', (req,res) => {

            res.render('compose');

        });

    // app.post requests (/)
        app.post('/', (req,res) => {

            

        });

    // app.post requests (/compose)
        app.post( '/compose', (req,res) => {

          const Journal = {

            Title: req.body.JournalTitle,
            Content: req.body.JournalContent,
            URL: _.kebabCase(req.body.JournalTitle)

          }

          JournalPosts.push(Journal);

          res.redirect('/');

        });

// ------------------------------------------------------

// port listener ----------------------------------------

    app.listen(Port, () => {
      console.log("Server started on port " + Port);
    });

// ------------------------------------------------------
